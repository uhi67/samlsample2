<?php


namespace app\components;


use yii\base\BaseObject;
use yii\web\IdentityInterface;

class DummyUser extends BaseObject implements IdentityInterface {
	public $id;
	public $attributes;

	public static function findIdentity($id) {
		return null;
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		return null;
	}

	public function getId() {
		return $this->id;
	}

	public function getAuthKey() {
		return md5(json_encode($this->attributes));
	}

	public function validateAuthKey($authKey) {
		return false;
	}
}