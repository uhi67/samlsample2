<?php
namespace app\components;
use Closure;
use SimpleXMLElement;
use yii\helpers\BaseArrayHelper;

/**
 * ArrayHelper provides additional array functionality that you can use in your
 * application.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ArrayHelper extends BaseArrayHelper {
	/**
	 * Az implode-hez hasonlóan, de indexxel együtt fűzi össze az értékeket.
	 * A kulcs és az érték között
	 * Rekurzív esetben a ragasztó lehet tömb, akkor szintenként a következő elemet használja (vagy az utolsót, ha elfogyott)
	 *
	 * @param mixed $glue1 -- értékek közötti delimiter
	 * @param mixed $glue2 -- kulcs és érték közötti delimiter
	 * @param mixed $array -- tömb (lehet rekurzív)
	 * @param array|null $recurse -- rekurzió esetén tömbelem külső határolói (kételemű tömb, vagy kételemű tömbök tömbje)
	 * @return string
	 */
	public static function implodeWithKeys($glue1, $glue2, $array, $recurse=null) {
	    $output = array();
	    $g1 = is_array($glue1) ? $glue1[0] : $glue1;
	    $g2 = is_array($glue2) ? $glue2[0] : $glue2;
	    foreach( $array as $key => $item ) {
	    	if(is_array($item) && $recurse) {
	    		$glue1x = $glue1; if(is_array($glue1x) && count($glue1x)>1) array_shift($glue1x);
	    		$glue2x = $glue2; if(is_array($glue2x) && count($glue2x)>1) array_shift($glue2x);
   				$recursex = $recurse;
	    		if(is_array($recurse)) {
	    			if(is_array($recurse[0])) {
	    				$r1 = $recurse[0][0]; $r2 = $recurse[0][1];
	    				if(count($recursex)>1) array_shift($recursex);
	    			}
	    			else {
	    				$r1 = $recurse[0]; $r2 = $recurse[1];
	    			}
	    		}
    			else {
    				$r1 = '['; $r2 = ']';
    			}
	    		$item = $r1. self::implodeWithKeys($glue1x, $glue2x, $item, $recursex). $r2;
	    	}
	    	$output[] = $key . $g2. $item;
        }
		return implode($g1, $output);
	}

	/**
	 * Like vsprintf, but accepts $args keys instead of order index.
	 * Both numeric and strings matching /[a-zA-Z0-9_-]+/ are allowed.
	 *
	 * Example: vskprintf('y = %y$d, x = %x$1.1f', array('x' => 1, 'y' => 2))
	 * Result:  'y = 2, x = 1.0'
	 *
	 * $args also can be object, then it's properties are retrieved
	 * using get_object_vars().
	 *
	 * '%s' without argument name works fine too. Everything vsprintf() can do
	 * is supported.
	 *
	 * @author Josef Kufner <jkufner(at)gmail.com>
	 *
	 * @param $str
	 * @param $args
	 *
	 * @return string
	 */
	public static function vksprintf($str, $args)
	{
	    if (is_object($args)) {
	        $args = get_object_vars($args);
	    }
	    $map = array_flip(array_keys($args));
	    $new_str = preg_replace_callback('/(^|[^%])%([a-zA-Z0-9_-]+)\$/',
	            function($m) use ($map) { return $m[1].'%'.($map[$m[2]] + 1).'$'; },
	            $str);
	    return vsprintf($new_str, $args);
	}

	/**
	 * Returns false is needle array is a subset of haystack.
	 * The search is recursive, and the order of numbered elements are arbitrary.
	 * Returns list of indices if difference is found (first difference only)
	 *
	 * @param array $needle
	 * @param array $haystack
	 * @param array $level
	 *
	 * @return boolean|array -- false on success, index-list if differs
	 */
	public static function isSubsetOf($needle, $haystack, $level=[]) {
		foreach($needle as $key=>$value) {
			$l = $level; $l[]=$key;
			if(is_int($key)) {
				$f = false;
				foreach($haystack as $k=>$v) {
					if(is_int($k)) {
						if(is_array($value)) {
							if(!self::isSubsetOf($value, $v, $l)) { $f=true; break; }
						}
						else {
							if($v==$value) { $f=true; break; }
						}
					}
				}
				if(!$f) return $l;
			}
			else {
				if(!isset($haystack[$key])) return $l;
				if(is_array($value)) {
					if(!is_array($haystack[$key])) return $l;
					$d = self::isSubsetOf($value, $haystack[$key], $l);
					if($d) return $d;
				}
				else {
					if($haystack[$key] != $value) return $l;
				}
			}
		}
		return false;
	}

	/**
	 * Converts SimpleXml object to array
	 *
	 * @param SimpleXMLElement $xml
	 * @return array -- root node attributes and content. Named elements are attributes (strin values) and subnode arrays (array values), numeric indexes are text nodes.
	 * @example
 	 *		SimpleXmlToArray(simplexml_load_string('<root><a a1="aaa1"><k k1="kk1">körte</k>alma</a><a />gyökér</root>'))
	 *  results
	 *		[
	 *			'a'=>[
	 *			 	['a1'=>'aaa1', 'k'=>[['k1'=>'kk1', 'körte']], 'alma' ],
	 *			 	[],
	 *			 ],
	 *			'gyökér'
	 *		]
	 *	],
	 *
	 */
	public static function SimpleXmlToArray($xml) {
		$result = array();
		foreach($xml->attributes() as $key=>$val) {
		    $result[(string)$key] = (string)$val;
		}
		foreach($xml->children() as $i=>$c) {
			$cr = self::SimpleXmlToArray($c);
		    $result[$c->getName()][] = $cr;
		}
		$s = $xml->__toString();
		if($s) $result[]=$s;
		return $result;
	}

	/**
	 * Replaces all occourences of all keys of $rules array to it's values using PCRE in $aa array recursively
	 *
	 * @param array|string $aa
	 * @param array $rules -- pattern=>replace
	 * @return array -- the array with replaced values
	 */
	public static function rreplace($aa, $rules) {
		if(is_array($aa)) foreach($aa as $i=>$a) $aa[$i] = self::rreplace($a, $rules);
		else if(is_string($aa)) $aa = preg_replace(array_keys($rules), array_values($rules), $aa);
		return $aa;
	}

	/**
	 * Returns first element which satisfies user function.
	 * Returns null if item is not found.
	 *
	 * @param array $array
	 * @param callable $test -- function($item, $index)
	 * @return mixed|null
	 */
	public static function array_find($array, $test) {
		foreach ($array as $i=>$item) {
			if(call_user_func($test, $item, $i)) return $item;
		}
		return null;
	}

	/**
	 * Returns named element of an array or object.
	 * If value is callable, retuns result evaluated with parameter $item.
	 * @param array|object $array -- array or object to search in
	 * @param string|callable $key -- key or callable as in {@see getValue()}
	 * @param mixed $default -- default value if key not present in array
	 * @param mixed $item -- parameter for callable value
	 * @return mixed
	 */
	public static function getValueEval($array, $key, $default=null, $item=null) {
		$value = static::getValue($array, $key, $default);
		if($value instanceof Closure) {
			$value = $value($item);
		}
		return $value;
	}

	/**
	 * Check the given array if it is an associative array.
	 *
	 * If `$strict` is true, the array is associative if all its keys are strings.
	 * If `$strict` is false, the array is associative if at least one of its keys is a string.
	 *
	 * An empty array will be considered associative only in strict mode.
	 *
	 *    - `isAssociative(array, false)` means the array has associative elements,
	 *    - `!isAssociative(array, true)` means the array has non-associative elements.
	 *
	 * @param array $array the array being checked
	 * @param bool $strict the array keys must be all strings and not empty to be treated as associative.
	 * @return bool the array is associative
	 */
	public static function isAssociative($array, $strict=true) {
		if (!is_array($array)) return false;
		foreach ($array as $key => $value) {
			if (!is_string($key) && $strict) return false;
			if (is_string($key) && !$strict) return true;
		}
		return $strict;
	}

	/**
	 * The classic map function with two additional feature:
	 *
	 * - if $from is null, original keys are preserved.
	 * - if $to is null, the original objects are preserved
	 *
	 * ----
	 * {@inheritDoc}
	 *
	 * @param array|object $array
	 * @param Closure|string|null $from
	 * @param Closure|string|null $to
	 * @param null $group
	 *
	 * @return array
	 */
	public static function map($array, $from, $to=null, $group = null) {
		if($from!==null && $to!==null) return parent::map($array, $from, $to, $group);
		$result = [];
		foreach ($array as $key => $element) {
			$key = $from===null ? $key : static::getValue($element, $from);
			$value = $to===null ? $element : static::getValue($element, $to);
			if ($group !== null) {
				$result[static::getValue($element, $group)][$key] = $value;
			} else {
				$result[$key] = $value;
			}
		}
		return $result;
	}

	/**
	 * Returns true if two array contains only equal elements
	 * Ignores array keys.
	 *
	 * @param array $a
	 * @param array $b
	 * @return bool
	 */
	public static function isEquals($a, $b) {
		// Normalize
		$aa = array_map(function($aaa) { return AppHelper::objtostr($aaa); }, $a);
		$bb = array_map(function($bbb) { return AppHelper::objtostr($bbb); }, $b);

		return array_diff($aa, $bb) === [] && array_diff($bb, $aa) === [];
	}

	/**
	 * Removes duplicate items from the array using $compare function to compare elements.
	 * Ignores, but keeps non-integer indices of returned elements.
	 *
	 * @param array $array -- the input array
	 * @param callable $compare -- callable($a, $b) returns 0 if elements are equal
	 * @return array -- the array without duplicate elements
	 */
	public static function uunique($array, $compare) {
		$result = [];
		foreach($array as $key=>$item) {
			// Ha $item nincs benne result-ban, hozzáadjuk.
			if(!static::array_find($result, function($value) use($item, $compare) {
				return $compare($value, $item) == 0;
			})) {
				if(is_int($key)) $result[] = $item;
				else $result[$key] = $item;
			}
		}
		return $result;
	}

	/**
	 * Fetches and removes value by the key from the array
	 *
	 * @param array $array -- modified when the $key found
	 * @param string $key
	 * @param mixed $default -- returned when $key does not exist in the array
	 */
	public static function fetch(&$array, $key, $default=null) {
		if(array_key_exists($key, $array)) {
			$value = $array[$key];
			unset($array[$key]);
			return $value;
		}
		return $default;
	}
}
