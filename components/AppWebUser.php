<?php


namespace app\components;

use yii\web\User;

/**
 * Class AppWebUser
 *
 * @property-read bool $isSysAdmin
 */
class AppWebUser extends User {
	private $_isSysAdmin = null;

	public function getIsSysAdmin() {
		if($this->_isSysAdmin === null) $this->_isSysAdmin = $this->can('System Administrator');
		return $this->_isSysAdmin;
	}
}
