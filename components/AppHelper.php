<?php
namespace app\components;

use Closure;
use DateTime;
use DateTimeZone;
use Exception;
use IntlDateFormatter;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Html;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\i18n\Formatter;

class AppHelper {
	const VOWELS = 'aáeéiíoóöőuúüű';
	public static $oo = [];
	/** @var array $environment */
	static $environment;
	/** @var string $envFile */
	static $envFile;

	private static $_version;

	/**
	 * Reads a config value from "params" set, with default.
	 * Missing value without default raises Exception.
	 *
	 * @param string $name
	 * @param string $default
	 * @return mixed
	 * @throws Exception
	 */
	static function getParam($name, $default='<error>') {
		$result = isset(Yii::$app->params[$name]) ? Yii::$app->params[$name] : $default;
		if($result === '<error>') throw new Exception(Yii::t('app', "Configuration error. Missing parameter '{param}'", ['param'=>$name]));
		return $result;
	}

	/**
	 * Debug dump instead of print_r
	 * Terminates.
	 *
	 * @param mixed $var
	 * @param mixed $caller
	 * @param bool $return
	 * @param int $depth
	 * @return string -- if ever
	 */
	static function d($var, $caller=null, $return=false, $depth=10) {
		$r = '';
	    if(is_numeric($caller)) {
	    	$bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $caller+1);
	        $caller = $bt[$caller];
	    }
	    if(!isset($caller)){
	    	$bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1);
	        $caller = array_shift($bt);
	    }
	    $r .= '<code>File: '.$caller['file'].' / Line: '.$caller['line'].'</code>';
	    $r .= '<pre>';
	    $r .= VarDumper::dumpAsString($var, $depth, true);
	    $r .= '</pre>';
	    if($return) return $r;
	    echo $r;
	    exit;
	}

	/**
	 * Easier use of d() in a view
	 *
	 * @param mixed $var
	 * @return string -- rendered values content
	 */
	static function dd($var) {
		return self::d($var, 1, true);
	}

	/**
	 * Debug multiple variables
	 * @param mixed ...$var
	 */
	static function trace(...$var) {
		foreach($var as $v)
			Yii::debug(VarDumper::dumpAsString($v, 10, false));
	}

	/**
	 * Translation helper for hungarian definite article
	 * returns 'z' if name's first sound is a vowel, empty otherwise.
	 *
	 * @param string $name
	 * @return string
	 * @throws Exception
	 */
	static function z($name) {
		if($name=='') return '';
		if(!mb_check_encoding($name, mb_internal_encoding())) throw new Exception('Invalid character encoding '.$name);
		return (mb_strpos(self::VOWELS, mb_strtolower(mb_substr($name,0,1)))===false) ? '' : 'z';
	}

	/**
	 * Olvasható stringgé alakít egy tömböt vagy objektumot, rekurzívan.
	 *
	 * @param mixed $obj
	 * @param bool $quotestrings -- a string értékeket '-ok közé teszi
	 * @param bool $ident -- formatted output with tabulator-idented lines
	 * @param int|null $depthlimit -- null = no limit (default)
	 * @return string -- tömböket [], objektumokat {} között ad vissza
	 */
	static function objtostr($obj, $quotestrings=false, $ident=false, $depthlimit=null) {
		if($depthlimit===0) return '{...}';
		if(!is_null($depthlimit)) $depthlimit--;
		if(is_null($obj)) return 'null';
		if(is_bool($obj)) return $obj ? 'true': 'false';
		if(is_int($obj)) return $obj;
		if(is_float($obj)) return $obj;
		if($ident===1 or $ident===true) $ident = "   ";
		$qs = ($quotestrings ? "'" :'');
		if(is_string($obj)) {
			if($quotestrings) $obj = str_replace("'", "\\'", str_replace("\\", "\\\\", $obj));
			return $qs.$obj.$qs;
		}
		if(is_array($obj)) {
			$result = '';
			$lasti = -1;
			foreach( $obj as $key => $item ) {
				if($result!='') $result.=', ';
				if($ident) $result .= "\n".$ident;

				if(is_integer($key)) {
					if($key==$lasti+1) $kx = ''; else $kx = $key.'=>';
					$lasti = $key;
				}
				else $kx = $qs.$key.$qs.'=>';

				$result .= $kx.self::objtostr($item, $quotestrings, $ident.$ident, $depthlimit);
			}
			return '['.$result.']';
		}
		if(is_object($obj)) {
			$result = '';
			foreach( $obj as $key => $item ) {
				if($result!='') $result.=',';
				if($ident) $result .= "\n".$ident;
				$result .= $qs.$key.$qs.'=>'.self::objtostr($item, $quotestrings, $ident.$ident, $depthlimit);
			}
			return '{'.$result.'}';
		}
		return $obj;
	}

	/**
	 * Jelenlegi időpont MySQL formátumban
	 *
	 * @return string
	 * @throws InvalidConfigException
	 * @throws Exception
	 */
	static function now() {
		return Yii::$app->formatter->asDateTime(new DateTime('NOW', new DateTimeZone('Europe/Budapest')), 'yyyy-MM-dd HH:mm:ss');
	}

	/**
	 * Conditional translate
	 * Translates only if text begins with "* "
	 *
	 * @param string $category
	 * @param string $text
	 * @return string
	 */
	static function tc($category, $text) {
		if(substr($text, 0, 2)=='* ') return Yii::t($category, substr($text,2));
		else return $text;
	}

	/**
	 * @param string $message
	 * @param string $class
	 */
	public static function sendMessage($message, $class='info') {
		if(Yii::$app->session)
			Yii::$app->session->addFlash('alerts', [$message, $class]);
	}

	/**
	 * Renders all flash messages in separate divs
	 * Handles array messages also.
	 * @return string
	 */
	public static function showMessages() {
		$flashes = Yii::$app->session->getFlash('alerts');
		if($flashes) {
			return Html::tag('div', implode(array_map(function($m) {
				$message = is_array($m) ? $m[0] : $m;
				$class = is_array($m) ? $m[1] : 'info';
				return Html::tag('div', $message, ['class'=>'alert alert-'.$class]);
			}, $flashes)), ['class'=>'messages no-print']);
		}
		else return '';
	}

	/**
	 * @param int|DateTime|string $date -- unix ts or DateTime or datetime string
	 * @param string $format -- default is short (2019-12-05)
	 * @return string
	 * @throws Exception
	 * @noinspection PhpUnused
	 * @see Formatter::asDate()
	 *
	 */
	public static function formatDate($date, $format='short') {
		$formatter = Yii::$app->formatter;
		try {
			return $formatter->asDate($date, $format);
		}
		catch(InvalidConfigException $e) {
			return (new DateTime($date))->format('Y-m-d');
		}
	}

	/**
	 * @see Formatter::asDatetime()
	 *
	 * @param int|DateTime|string $date -- unix ts or DateTime or datetime string
	 * @param string $format -- default is short (2019-12-05 14:49:10)
	 * @return string
	 * @throws Exception
	 */
	public static function formatDateTime($date, $format='short') {
		$formatter = Yii::$app->formatter;
		try {
			return $formatter->asDatetime($date, $format);
		}
		catch(InvalidConfigException $e) {
			return (new DateTime($date))->format('Y-m-d H:i:s');
		}
	}

	/**
	 * Returns field format array for DetailView
	 *
	 * options:
	 *
	 *  - attribute - string -- link id value
	 *  - idField - string
	 *  - link - string|array -- may contain {id}
	 *  - nameField --
	 *  - enabled -- bool -- link is enabled, default is true
	 *
	 * @param ActiveRecord $model
	 * @param string $reference -- reference name or null. May be dotted subreferenced format
	 * @param array $options
	 *
	 * @return array
	 * @throws Exception
	 */
	public static function linkFormat($model, $reference, $options=[]) {
		$referenceMap = explode('.', $reference);
		$lastReference = Inflector::camel2id($referenceMap[count($referenceMap)-1]);
		$controller = ArrayHelper::getValue($options, 'controller', $lastReference);
		$link = ArrayHelper::getValue($options, 'link', [$controller.'/{id}']);
		$attrRefMap = $referenceMap;
		$attrRefMap[count($referenceMap)-1] = Inflector::camel2id($referenceMap[count($referenceMap)-1], '_').'_id';
		$attribute = ArrayHelper::getValue($options, 'attribute', implode('.', $attrRefMap));
		$nameField = ArrayHelper::getValue($options, 'nameField', 'name');
		$idField = ArrayHelper::getValue($options, 'idField', $reference.'.id');
		$enabled = ArrayHelper::getValue($options, 'enabled', true);

		if(is_array($link)) $link = Url::to($link);

		$ref = ArrayHelper::getValue($model, $reference);
		$id = ArrayHelper::getValue($model, $idField);
		$text = $reference ? ($ref ? $ref->$nameField : null) : $model->$nameField;
		$value = $enabled && $text ? Html::a($text, str_replace('{id}', $id, $link)) : $text;
		return [
			'attribute' => $attribute,
			'format' => 'raw',
			'value' => $value
		];
	}

	/**
	 * Returns link field format array for GridView
	 *
	 * options:
	 *
	 *  - attribute - string -- default is `reference_id`
	 *  - id - string -- link id value
	 *  - controller -- default base for link, default is reference
	 *  - link - string|array -- may contain {id}, default is `controller/{id}`
	 *  - nameField -- default is `name`
	 *  - enabled -- callable(model)|bool -- link is enabled, default is true
	 *  - nullValue -- returned value if referenced value is null.
	 *  - columnOptions -- other options into the generated column definition (same keys override previous ones)
	 *
	 *  all other option keys are ignored.
	 *
	 * @param string $reference -- reference name or null
	 * @param array $options
	 *
	 * @return array
	 * @throws Exception
	 */
	public static function linkColumn($reference, $options=[]) {
		$attribute = ArrayHelper::getValue($options, 'attribute', $reference ? $reference.'_id' : 'name');
		$columnOptions = ArrayHelper::getValue($options, 'columnOptions');
		$contentOptions = ArrayHelper::getValue($options, 'contentOptions');
		$columnDef = [
			'attribute' => $attribute,
			'format' => 'raw',
			'value' => function($model) use($reference, $options) {
				$controller = ArrayHelper::getValue($options, 'controller', $reference);
				$link = ArrayHelper::getValue($options, 'link', [$controller ? $controller.'/{id}' : '{id}']);
				$id = ArrayHelper::getValue($options, 'id', $reference ? $reference.'_id' : 'id');
				$nameField = ArrayHelper::getValue($options, 'nameField', 'name');
				$nullValue = ArrayHelper::getValue($options, 'nullValue');
				if($reference && !$model->$reference) return $nullValue;
				$text = $reference ? $model->$reference->$nameField : $model->$nameField;
				$enabled = ArrayHelper::getValue($options, 'enabled', true);
				if(is_callable($enabled)) $enabled = call_user_func($enabled, $model);
				return $enabled ?
					Html::a($text, str_replace('{id}', $model->$id, $link)) :
					$text;
			},
		];
		if($columnOptions) $columnDef = array_merge($columnDef, $columnOptions);
		if($contentOptions) $columnDef['contentOptions'] = $contentOptions;
		return $columnDef;
	}

	/**
	 * Returns translated field format array for GridView
	 *
	 * @param string $attribute
	 * @param string $category
	 * @param array $columnOptions
	 *
	 * @return array
	 */
	public static function translatedColumn($attribute, $category='app', $columnOptions = []) {
		return array_merge([
			'attribute' => $attribute,
			'value' => function($item) use($category, $attribute) {
				return Yii::t($category, ArrayHelper::getValue($item, $attribute));
			}
		], $columnOptions);
	}

	/**
	 * Returns ICU standard datetime formatting string for the specified date or time variant.
	 *
	 * @param int|null $datetype -- use IntlDateFormatter::SHORT, etc. null = no date.
	 * @param int|null $timetype -- use IntlDateFormatter::SHORT, etc. null = no time.
	 * @return string --
	 */
	public static function datetimeformat($datetype=null, $timetype=null) {
		$locale = Yii::$app->language;
		$formatter = new IntlDateFormatter($locale, $datetype, $timetype);
		return $formatter->getPattern();
	}

	/**
	 * Converts ICU date-time pattern to momentjs format (partial solution)
	 * @param $format
	 * @return string
	 */
	public static function ICUToMomentFormat($format) {
		$replacements = [
			'y' => 'YYYY',      // year (full)
			'yyyy' => 'YYYY',
			'yy' => 'YY',       // year (2-digit)
			'M' => 'M',         // Month 1-digit
			'MM' => 'MM',       // Month 2-digit
			'MMM' => 'MMM',     // Month name short
			'MMMM' => 'MMMM',   // Month name long
			'MMMMM' => 'M',     // Month letter*

			'd' => 'D',         // day in month (1-digit)
			'dd' => 'DD',       // day in month (2-digit)
			'D' => 'DDD',       // day of year
			'E' => 'ddd',       // day of week (short name)
			'EE' => 'ddd',      // day of week (short name)
			'EEE' => 'ddd',     // day of week (short name)
			'EEEE' => 'dddd',   // day of week (long name)
			'EEEEE' => 'ddd',   // day of week (letter)*
			'EEEEEE' => 'ddd',  // day of week (2-letter)*
			'e' => 'E',         // day of week (1-digit 1-7)
			'ee' => 'E',        // day of week (2-digit 01-07)*

			'h' => 'h',        // hour in am/pm (1~12)
			'hh' => 'hh',      // hour in am/pm (1~12)
			'H' => 'H',        // hour in day (0~23)
			'HH' => 'HH',      // hour in day (0~23)
			'm' => 'm',        // minute in hour
			'mm' => 'mm',      // minute in hour
			's' => 's',        // second in minute
			'ss' => 'ss',
			'S' => 'SS',       // Fractional seconds
			'SS' => 'SS',      // Fractional seconds
			'SSS' => 'SS',     // Fractional seconds

			'Z' => 'Z',        // Offset from UTC as +-HH:mm, +-HHmm, or Z
			'ZZ' => 'ZZ',
			'ZZZ' => 'ZZ',
		];
		return strtr($format, $replacements);
	}

	public static function base32_encode($hash) {
		$validChars = array(
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', //  7
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 15
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 23
			'Y', 'Z', '2', '3', '4', '5', '6', '7', // 31
			'=',  // padding char
		);
		$secret = '';
		for ($i = 0; $i < strlen($hash); ++$i) {
			$secret .= $validChars[ord($hash[$i]) & 31];
		}
		return $secret;
	}

	/**
	 * Google Authenticatorral kompatibilis ellenőrző kód előállítása
	 *
	 * @param string $secret
	 * @param string $uid
	 * @param string $tfaSalt
	 * @param int $length
	 * @return string
	 */
	public static function userSecret($secret, $uid, $tfaSalt, $length=16) {
		return AppHelper::base32_encode(substr(hash('sha256', $secret.$uid.$tfaSalt, true), 0, $length));
	}

	/**
	 * Csinál egy akciógombot, mely a kívánt címre ugrik, kivéve, ha megadtunk egy jelszóűrlapot, akkor azt aktiválja
	 *
	 * - visible
	 * - enabled
	 * - caption
	 * - url
	 * - class
	 * - title
	 * - confirm
	 * - form
	 * - action
	 * - icon
	 * - header
	 * - target
	 * - extra
	 *
	 * @param array $options -- beállítások
	 *
	 * @return string
	 * @throws Exception
	 */
	public static function actionButton(array $options) {
		$visible = ArrayHelper::getValue($options, 'visible');
		$enabled = ArrayHelper::getValue($options, 'enabled');
		$caption = ArrayHelper::getValue($options, 'caption');
		$icon = ArrayHelper::getValue($options, 'icon');
		$color = ArrayHelper::getValue($options, 'color', 'primary');
		$class = ArrayHelper::getValue($options, 'class', $icon ? 'btn btn-'.$color.' '.$icon : '');
	 	$title = ArrayHelper::getValue($options, 'title');
		$confirm = ArrayHelper::getValue($options, 'confirm');
		$form = ArrayHelper::getValue($options, 'form');
		$header = ArrayHelper::getValue($options, 'header');
		$action = ArrayHelper::getValue($options, 'action');
		$id = ArrayHelper::getValue($options, 'id');
		$url = Url::to(ArrayHelper::getValue($options, 'url', $id ? [$action, 'id' => $id] : ''));
		$extra = ArrayHelper::getValue($options, 'extra');
		static::$oo[$action] = $options;

		if(!$visible) return '';
		return Html::a($caption, $form ? null : $url, [
			'class' => $class . ($enabled ? '' : ' disabled') . ($form ? ' run-action-form' : ''),
			'title' => $title,
			'target' => ArrayHelper::getValue($options, 'target'),
			'data' => [
				'confirm' => $form ? null: $confirm,
				'body' => $form ? $confirm : null,
				'method' => 'post',
				'form' => $form,
				'action' => $action,
				'header' => $header,
				'url' => $url,
				'extra' => $extra,
			],
		]);
	}

	/**
	 * Waits for a test to satisfy
	 *
	 * @param Closure $test -- test to run. Must return true on success.
	 * @param int $timeout -- timeout in sec
	 * @param int $interval -- test interval in sec
	 * @return bool -- true if test succeeded within timeout, false otherwise
	 */
	public static function waitFor($test, $timeout=60, $interval=1) {
		$startTime = time();
		do {
			$lastTry = time();
			if($test()) return true;
			/** @noinspection PhpStatementHasEmptyBodyInspection */
			while(time()>$lastTry+$interval);
		}
		while(time() < $startTime+$timeout);
		return false;
	}

	/**
	 * @param $time
	 *
	 * @return string|string[]
	 * @throws InvalidConfigException
	 */
	public static function fullTime($time) {
		return str_replace('.,', '.', Yii::$app->formatter->asDate($time, 'full') . ' ' . Yii::$app->formatter->asTime($time, 'short'));
	}

	/**
	 * Visszaadja az alkalmazás aktuális verziószámát (fájlból vagy gitből)
	 * @return string
	 */
	public static function version() {
		if(!self::$_version) {
			$versionFile = dirname(__DIR__) . '/version';
			self::$_version = file_exists($versionFile) ? trim(file_get_contents($versionFile)) : '';
			if(!self::$_version) {
				$version = trim(exec('git describe --tags --abbrev=1'));
				if($version) file_put_contents(dirname(__DIR__).'/version', self::$_version = $version);
			}
		}
		return self::$_version;
	}

	/**
	 * Converts markdown notation to html.
	 *
	 * If begin/end pattern is specified, conversion will be performed in all fragments delimited by begin/end or in the entire content otherwise.
	 *
	 * @param string $content -- content to replace markdown in
	 * @return string
	 */
	public static function convertMarkdown($content) {
		$rules = [
			['~(\s)[*_]([^*]+)[*_](\s)~', '$1<i>$2</i>$3'],
			['~(\s)[*_]{2}([^*]+)[*_]{2}(\s)~', '$1<b>$2</b>$3'],
			['~^# ?(.*)$~', '<h1>$1</h1>'],
			['~^## ?(.*)$~', '<h2>$1</h2>'],
			['~^### ?(.*)$~', '<h3>$1</h3>'],
			['~^#### ?(.*)$~', '<h4>$1</h4>'],
		];
		$content = preg_replace(array_column($rules, 0), array_column($rules, 1), $content);
		return $content;
	}

	/**
	 * @param string $current
	 * @param string $limit
	 * @param string $operator ('<', '>', '=', '==', '<=', '>=', '<>', '!=', '~')
	 * @param array $cc -- version tag values
	 *
	 * @return bool
	 */
	public static function compareVersion($current, $limit, $operator, &$cc=[]) {
		$vre = '~(\d+)\.(\d+)(?:\.(\d+))?(?:\.(\d+))?(?:\-(\d+))?~'; // compare $1, $2, $3, $4, $5
		if($operator != '~' && preg_match($vre, $limit, $ll)  && preg_match($vre, $current, $cc)) {
			for($i=1; $i<count($cc); $i++) {
				$c = $cc[$i];
				$l = isset($ll[$i]) ? $ll[$i] : '';
				switch($operator) {
					case '>':
					case '>=':
						if($c > $l) return true;
						if($c < $l) return false;
						break;
					case '=':
					case '==':
						if($c != $l) return false;
						break;
					case '<':
					case '<=':
						if($c < $l) return true;
						if($c > $l) return false;
						break;
					case '<>':
					case '!=':
						if($c != $l) return true;
				}
			}
		}
		else {
			switch($operator) {
				case '>=':
					return $current >= $limit;
				case '=':
				case '==':
					return $current == $limit;
				case '<=':
					return $current <= $limit;
				case '>':
					return $current > $limit;
				case '<':
					return $current < $limit;
				case '<>':
				case '!=':
					return $current != $limit;
				case '~':
					return !!preg_match('~' . $limit . '~', $current);
			}
		}
		return !in_array($operator, ['!=', '<>', '<', '>']);
	}

}
