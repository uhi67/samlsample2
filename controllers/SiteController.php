<?php /** @noinspection PhpUnused */

namespace app\controllers;

use app\components\AppHelper;
use app\components\ArrayHelper;
use app\components\DummyUser;
use uhi67\eduidsp\Saml;
use uhi67\envhelper\EnvHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SiteController extends AppController {

	/**
	 * Displays homepage.
	 *
	 * @return string
	 * @throws InvalidConfigException
	 */
    public function actionIndex() {
        return $this->render('index');
    }

	/**
	 * Logout action.
	 *
	 * @return Response
	 * @throws
	 */
    public function actionLogout() {
		Yii::$app->user->logout(true);
		if(Yii::$app->saml->isAuthenticated()) Yii::$app->saml->logout();
		return $this->goHome();
    }

	/**
	 * Login action.
	 *
	 * @return Response|string
	 * @throws \Exception
	 * @noinspection PhpFullyQualifiedNameUsageInspection
	 */
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$saml = $this->saml;
		if(!$saml) {
			AppHelper::sendMessage('Nincs konfigurálva a belépési szolgáltatás!', 'danger');
			return $this->redirect('/');
		}

		$idp = EnvHelper::getEnv('idp', null);
		$disco = EnvHelper::getEnv('disco', null);
        $baseUrl = EnvHelper::getEnv('APP_BASEURL', null);
		$params = [
            'ReturnTo' => $baseUrl,
        ];
		if($idp && $disco) $params['saml:idp'] = $idp;
		if(!$saml->isAuthenticated()) $saml->requireAuth($params);

		$attributes = $saml->getAttributes();
		$id = ArrayHelper::getValue($attributes, $saml->idAttribute);
		if(is_array($id)) $id = $id[0];
		$user = new DummyUser([
			'id' => $id,
			'attributes' => $attributes
		]);
		Yii::$app->user->login($user, 8*60*60);
		return $this->redirect('/');
	}

	/**
	 * @param string|null $id -- dokumentációs oldal szöveges azonosítója
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionDoc($id=null) {
		if(!$id) $id = 'index';
		$docfile = dirname(__dir__)."/doc/$id.md";
		if(!file_exists($docfile)) throw new NotFoundHttpException("Az `$id` oldal nem található");

		return $this->render('doc', [
			'doc' => file_get_contents($docfile),
		]);
	}

	public function actionOwn() {
		/** @var Saml $saml */
		return $this->render('own', [
			'user' => $this->appUser,
			'isAuth' => $this->saml->isAuthenticated(),
			'attributes' => $this->saml->attributes,
		]);
	}
}
