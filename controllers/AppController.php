<?php

namespace app\controllers;

use app\components\AppHelper;
use app\components\AppWebUser;
use app\components\ArrayHelper;
use app\components\DummyUser;
use Exception;
use uhi67\eduidsp\Saml;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use yii\web\YiiAsset;

/**
 * Class AppController
 *
 * - Retrieves current user display data into $this->user
 * - Creates application's main menu
 *
 * @property-read Saml $saml
 */
class AppController extends Controller {
	public $mainMenu = [];
	public $userPanel;	// render data for login panel
	/** @var int $itemid value of id parameter */
	public $itemid;

	/** @var DummyUser $appUser -- the logged in appUser or null */
	public $appUser;
	/** @var AppWebUser $user -- the User component of Yii */
	public $user;

	/**
	 * @param Action $action
	 *
	 * @return bool
	 * @throws
	 */
	public function beforeAction($action) {
		if (!parent::beforeAction($action)) {
			return false;
		}

		$this->user = Yii::$app->user;
		$this->appUser = $this->user->identity;
		$this->itemid = Yii::$app->request->get('id');

    	// Login sáv adatainak előállítása
		$this->userPanel = [
			'isauthenticated' => !!$this->appUser,
			'displayname' => $this->appUser ? $this->appUser->displayname : 'Not logged in',
			'enabled' => $this->appUser ? $this->appUser->enabled : false,
			'id' => $this->appUser ? $this->appUser->id : null,
		];

		// Naplózás
		if(!in_array($this->action->uniqueId, [])) $this->logAccess();

		return true; // or false to not run the action
	}

	/**
	 * @param string $view
	 * @param array $params
	 *
	 * @return string
	 * @throws InvalidConfigException
	 */
	public function render($view, $params = []) {
		$saml = $this->saml;
		$mainMenu = [
		];

		if($saml && $saml->isAuthenticated()) {
			$loginMenu = [
				[
					'enabled' => true,
					'caption' => $saml->get('displayName', 0),
					'title' => 'Személyes menü',
					'class' => '',
					'action' => '/user/own',
					'icon' => 'fa-user',
					'items'=> [
						[
							'enabled' => true,
							'caption' => 'Saját adataim',
							'class' => '',
							'action' => '/user/own',
							'icon' => 'fa-user',
						],
						[
							'enabled' => true,
							'caption' => 'Kilépés',
							'action' => '/logout',
							'icon' => 'fa-sign-out-alt',
						]
					]
				],
			];
			$mainMenu = array_merge($mainMenu, [
				[
					'enabled' => 1,
					'caption' => 'Dokumentáció',
					'icon' => 'fa-book',
					'title' => 'Felhasználói útmutató',
					'action' => ['doc/index'],
				]
			]);
		}
		else {
			$loginMenu[] =
				[
					'enabled' => true,
					'caption' => Yii::t('yii', 'Login'),
					'action' => '/login',
				];
		}

		$this->mainMenu = array_merge($mainMenu, $this->mainMenu, [
			/*
			// Nyilvános menüpontok, ha vannak
				[
					'enabled' => 1,
					'caption' => 'Dokumentáció',
					'icon' => 'fa-book',
					'title' => 'Felhasználói útmutató',
					'action' => $saml->isAuthenticated() ? '/doc' : '/doc/view',
				]
			*/
			], $loginMenu);

		$params['appUser'] = $this->appUser;
		$params['user'] = $this->user;

		if(file_exists(Yii::getAlias('@webroot/js/'.$this->id.'.js')))
			$this->getView()->registerJsFile('@web/js/'.$this->id.'.js?ver='.AppHelper::version(), ['depends'=>[YiiAsset::class]]);

		return Parent::render($view, $params);
	}

	/**
	 * Returns authentication source
	 *
	 * @return Saml
	 */
	public function getSaml() {
		/** @noinspection PhpUndefinedFieldInspection */
		return Yii::$app->has('saml') ? Yii::$app->saml : null;
	}

	/**
	 * @param string|array|null $message
	 * @param array|null $params
	 * @param string $category
	 *
	 * @throws Exception
	 */
	public function logAccess($message=null, $params=null, $category='app-access') {
		$action = $this->action;
		$param = Yii::$app->request->queryParams;
		if(is_array($params)) $param = array_merge($param, $params);
		Yii::info(array_filter([
			'page' => $action->controller->uniqueId,
			'act' => $action->id,
			'target' => is_numeric($this->itemid) ? $this->itemid : ArrayHelper::getValue($param, 'id'),
			'message' => $message,
			'param' => $param
		], function($v) { return $v !== null; }), $category);
	}

	/**
	 * @param string|array|null $message
	 * @param array|null $params
	 *
	 * @throws Exception
	 */
	public function logAction($message, $params=null) {
		$this->logAccess($message, $params, 'app-action');
	}
}
