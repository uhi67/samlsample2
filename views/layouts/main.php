<?php /** @noinspection PhpUnhandledExceptionInspection */

/** @var $this View */
/** @var $content string */

use app\assets\FishAsset;
use app\components\AppHelper;
use app\widgets\Alert;
use uhi67\eduidsp\SamlAsset;
use uhi67\umenu\UMenu;
use uhi67\umenu\UMenuAsset;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
UMenuAsset::register($this);
SamlAsset::register($this);
$fishAsset = FishAsset::register($this);

$brandName = mb_strtoupper('My Company');
$appName = mb_strtoupper(Yii::$app->name);

$footerBrand = AppHelper::getParam('footerBrand', 'PTE');
$version = AppHelper::version();
$versionx = $version ? 'Verzió: ' . Html::a($version, ['site/doc', 'id'=>'version']) : '';
$mainMenu = $this->context->mainMenu;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Author" content="Uherkovich Peter">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap layout-main">
    <?php
		/** @noinspection PhpUnhandledExceptionInspection */
		$color = AppHelper::getParam('color', null);
		$options = ['class' => 'navbar-fixed-top app-top navbar-dark navbar-expand-md'];
		if($color) $options['style'] = ['background-color'=>$color];
		NavBar::begin([
			'brandLabel' => Html::tag('div',
			    Html::img($fishAsset->baseUrl . '/icons/ssplogo-fish-small.png') .
			    Html::tag('div',
					Html::tag('div', $brandName, ['class'=>'brand-header']).
			        Html::tag('div', $appName, ['class'=>'brand-slogan']),
			        ['class'=>'brand-name']),
			    ['class'=>'brand-group']),
			'brandUrl' => Yii::$app->homeUrl,
			'options' => $options,
		]);
    ?>
	<?=
		/** @noinspection PhpUnhandledExceptionInspection */
		Nav::widget([
			'options' => ['class' => 'navbar-nav navbar-right ml-md-auto'],
			'items' => (new UMenu(['items'=>$mainMenu]))->navItems(),
			'encodeLabels' => false,
		])
	?>
	<?php NavBar::end(); ?>

    <div class="container">
        <?= /** @noinspection PhpUnhandledExceptionInspection */
			Breadcrumbs::widget([
            	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= /** @noinspection PhpUnhandledExceptionInspection */
        	Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
		<p class="pull-left"><?= $versionx ?></p>
        <p class="pull-right">&copy; <?= $footerBrand ?> <?= date('Y') ?></p>
    </div>
</footer>

<div id="error-dialog" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Modal title</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
