<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $doc string */

$this->title = 'Organizational Voting System';
uhi67\showdown\ShowdownAsset::register($this);
$this->registerJsFile(Url::to(['/js/doc.js']), ['depends'=>'uhi67\showdown\ShowdownAsset']);
?>
<div class="site-index">
    <div class="body-content">
		<div class="alert alert-info">Kivonat a dokumentációból</div>
		<div class="showdown">
			<pre class="markdown"><?= $doc ?></pre>
		</div>
    </div>
</div>
