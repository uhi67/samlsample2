<?php /** @noinspection PhpUnhandledExceptionInspection */

use app\components\AppWebUser;
use app\components\ArrayHelper;
use uhi67\eduidsp\Saml;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $isAuth bool */
/* @var $attributes array */
/* @var $user AppWebUser */

$this->title = ArrayHelper::getValue($attributes, 'displayName', ArrayHelper::getValue($attributes, 'uid'));
if(is_array($this->title)) $this->title = $this->title[0];
$this->params['breadcrumbs'][] = 'Saját adatok';
YiiAsset::register($this);
?>
<div class="app-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(!$user->isGuest) : ?>
		<h2>Alkalmazásban tárolt adatok</h2>
		<p>Ez az alkalmazás nem tárol felhasználói adatokat.</p>
	<?php endif; ?>

	<?php if($isAuth) : ?>
		<h2>Azonosító szervezettől kapott adatok</h2>

		<?= /** @noinspection PhpUnhandledExceptionInspection */
		DetailView::widget([
			'options' => ['class'=>'table detail-view attributes'],
			'model' => array_map(function($a) {
				if(is_array($a) && count($a)>0 && is_array($a[0])) {
					$a = array_map(function($b) { return implode(', ', $b);}, $a);
				}
				if(is_array($a) && count($a) >1) return Html::ul($a);
				if(is_array($a) && count($a)==1) return $a[0];
				if(is_array($a) && count($a)==0) return 'nincs megadva';
				return $a;
			}, $attributes),
			'attributes' => array_map(function($key, $values) { return [
				'label' => Saml::translateAttributeName($key, Yii::$app->language),
				'format' => 'raw',
				'value' => Saml::formatAttribute($key, $values),
			];}, array_keys($attributes), array_values($attributes)),
		]) ?>
	<?php else : ?>
		<div class="alert alert-warning">Ön nincs belépve.</div>
	<?php endif; ?>

</div>
