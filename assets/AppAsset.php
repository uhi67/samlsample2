<?php
namespace app\assets;

use yii\bootstrap4\BootstrapAsset;
use yii\web\YiiAsset;

/**
 * Main application asset bundle.
 */
class AppAsset extends AppAssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
	    'css/font-roboto.css',
    ];
    public $js = [
		'js/site.js',
	];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
	    FontAwesomeAsset::class,
		FishAsset::class,
    ];
}
