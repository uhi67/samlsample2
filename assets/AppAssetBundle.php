<?php

namespace app\assets;

use app\components\AppHelper;
use yii\web\AssetBundle;

class AppAssetBundle extends AssetBundle {
	public function init() {
		parent::init();
		$this->js = array_map(function($item) { return $item.'?ver='.AppHelper::version(); }, $this->js);
	}
}
