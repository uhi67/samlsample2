<?php

namespace app\assets;

use yii\web\AssetBundle;

class FishAsset extends AssetBundle {
	public $sourcePath = '@vendor/simplesamlphp/simplesamlphp/www/assets';
}
