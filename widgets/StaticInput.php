<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * ## Class StaticInput
 * ### A display-only value without hidden input
 *
 * See also {@see \app\widgets\DisableInput}
 *
 * {@inheritdoc}
 *
 * @package app\widgets
 */
class StaticInput extends InputWidget {
	public function run() {
		echo Html::tag('div', $this->value, array_merge(['class'=>'form-control disabled'], $this->options));
	}
}
