<?php
namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * ## Class StaticInput
 * ### A display-only value with hidden input
 *
 * See also {@see \app\widgets\StaticInput}
 *
 * {@inheritdoc}
 *
 * @package app\widgets
 */
class DisabledInput extends InputWidget {
	public function run() {
		echo $this->renderInputHtml('hidden');
		echo Html::tag('div', $this->value, array_merge(['class'=>'form-control disabled'], $this->options));
	}
}
