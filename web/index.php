<?php

$env = getenv('APPLICATION_ENV');
$environment = isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : ($env ? $env : 'production');

if($environment == "local" ) {
	ini_set('display_errors', 'stdout');
	error_reporting(E_ALL);
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_ENV') or define('YII_ENV', 'local');
	defined('YII_ENV_DEV') or define('YII_ENV_DEV', true);
}
else if ($environment == "development") {
	ini_set('display_errors', 'stdout');
	error_reporting(E_ALL);
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_ENV') or define('YII_ENV', 'development');
	defined('YII_ENV_DEV') or define('YII_ENV_DEV', true);
}
else if ($environment == "production") {
	error_reporting(0);
	defined('YII_DEBUG') or define('YII_DEBUG', false);
	defined('YII_ENV') or define('YII_ENV', 'production');
}

if(!file_exists($vendorLoad = dirname(__DIR__) . '/vendor/autoload.php')) {
	echo "composer install needed.";
	exit;
}
require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/vendor/yiisoft/yii2/Yii.php';

$config = require dirname(__DIR__) . '/config/web.php';

/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
(new \yii\web\Application($config))->run();
