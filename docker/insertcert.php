<?php
if($argc<4) {
	echo "Usage: insertcert certfile targetfile [pattern] [replace]
	1. If no pattern given: Replaces {\\\$cert} to given certificate.
	2. If pattern is given, but not replace:
		- pattern must contain {\\\$cert} subpattern.
		- First replaces {\\\$cert} in pattern to the certificate
		- Then replaces pattern to substituted pattern in the output file. 
	3. If pattern and replace is given
		- Pattern must be a RegEx.	
		- First replaces pattern to replace. Replace must contain {\\\$cert}.
		- Then replaces {\\\$cert} to given certificate
	";
	exit;
}
$certfile = $argv[1];
$targetfile = $argv [2];
$pattern = $argc>3 ? $argv[3] : '';
$replace = $argc>4 ? $argv[4] : null;

$cert = preg_replace('/\s+|-+\w+\s*CERTIFICATE-----/', '', file_get_contents($certfile));
echo "Certfile: $certfile, \n";
echo "Targetfile: $targetfile, \n";

$from = '{$cert}';
$to = $cert;

$target = file_get_contents($targetfile);
if($replace !== null) {
	echo "Replacing '$pattern' to '$replace'\n";
	$target = preg_replace($pattern, $replace, $target);
} else if($pattern) {
	$from = preg_replace('~{\$cert}~', '', $pattern);
	$to = preg_replace('~{\$cert}~', $cert, $pattern);
}
echo "Replacing '$from' with current certificate.\n";
echo "Content: '$to'\n";
$target = str_replace($from, $to, $target);
file_put_contents($targetfile, $target);
