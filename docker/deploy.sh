#! /bin/bash
# Clone or update application from VCS
cd /app
if [ -d ".git" ]; then
    echo "Updating from VCS"
    git pull
    git describe --tags --abbrev=0 > version
    chown www-data:www-data version
    rm -rf web/assets/*
fi
if [ ! -d ".git" ] && [ ! -f "/app/yii" ] && [ "$VCS" != "" ]; then
    rm -rf /app/*
    rm -rf /app/.*
    echo "Cloning from VCS '$VCS'"
    git clone $VCS .
    git describe --tags --abbrev=0 > version
    chown www-data:www-data version
fi
if [ ! -d ".git" ] && [ ! -f "/app/yii" ] && [ "$VCS" = "" ]; then
    echo "Source is missing, please specify VCS to clone from"
fi
echo "Current version is $(<version)"
