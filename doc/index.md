Yii2 mintaprojekt composeres SimpleSAMLphp SP konfigurációval
=============================================================

1. Bevezetés
------------
A mintaprojekt egy "üres" php/composer alkalmazás a következő előkészített elemekkel:

- Yii2 keretrendszer
- SimpleSAMLphp authentikáció
- Bootstrap4, Font-awesome
- Composer post-install script
- Docker előkészítés konténer-inicializáló scripttel

Adatmodellt, adatbázis kapcsolatot nem tartalmaz.
A mintaprojekt telepítés után, a megfelelő környezeti változók beállítását követően futásra kész, szolgáltatásai:

- navbar címmel és menüvel
- login
- logout
- bejelentkezett felhasználó számára a saját attributumainak mutatása (GDPR transzparencia)
- verziószám
- change logindex.php

2. Telepítés
------------

### 3. Telepítés vhost környezetbe

**FONTOS**

A `composer install` első alkalommal, amikor a simplesamlphp még nincs telepítve a vendor alá, hibásan fut le.
A simplesamlphp modulokat a gyökér alá teszi be. A `/modules` mappát törölni kell, és újra futtatni a `composer install` parancsot.
Másodszorra jó lesz.

### 4. Telepítés Docker környezetbe


### Belső IdP-hez certificate generálás:

`openssl req -newkey rsa:3072 -new -x509 -days 3652 -nodes -out example.org.crt -keyout example.org.pem [-config config.csr]`

Environment variables
---------------------

Set environment variables in apache config or docker-compose.yml

Variable         | Description               | Default value or example
-----------------|---------------------------|--------------------------
APPLICATION_ENV  | environment type for YII2/simplesamlphp | "production"/"development"/"local"
SIMPLESAMLPHP_CONFIG_DIR | * place of simplesaml configuration
|
APP_NAME || myApp
APP_TITLE ||
|
SAML_ADMIN_PASSWORD | saml felület adminja | lehet akár kecske is
SAML_SECRET_SALT | any random secret string |
SAML_TECHNICALCONTACT_NAME | főleg idp-hez kell, nincs jelentősége
SAML_TECHNICALCONTACT_EMAIL | főleg idp-hez kell, nincs jelentősége
SAML_IDP | ha üres, discoveryt hív | default üres, pte: 'https://idp.pte.hu/saml2/idp/metadata.php'
SAML_DISCO | ha ez is üres, belső discovery | default üres, eduid: 'https://discovery.eduid.hu'
SAML_AUTH_SOURCE | jó, ha megegyezik az APP_NAME értékével | default értéke 'default-sp'
SAML_ID_ATTRIBUTE | SAML attribute to identify users | eduPersonPrincipalName

A környezeti változók kezelése
------------------------------
Ez a mintakonfiguráció az `uhi67\envhelper\EnvHelper` ("uhi67/envhelper") használatára van kialakítva, de más megoldást is használhatunk (akár egyszerű gyári getenv() is megteszi)

Az EnvHelper szolgáltatásai:

- A környezeti változókat alapvetően az OS/docker yaml/apache conf szentháromságból veszi, de ezt eddig a getenv() is tudja
- Ha szükséges, a környezeti változók a gyökérben lévő `.env` fájlban felülbírálhatók (ha nem férünk hozzá/macerás változtatni az eredeti forráson)
  Az `EnvHelper::init()` egyszeri hívása vagy az első `EnvHelper::getenv()` után már a sima `getenv()` hívások is "látják" az .env-ben megadott értékeket.
- A változóneveket kapitalizálja, például az 'appName' leékrdezés az 'APP_NAME' változót kérdezi le.
- A konfigurációkban lévő tömbök tömbösített lekérdezését egyszerűsíti (elemenkénti változónevekre való fordítással)
  Például a 'saml' tömb 'authSource' kulcsához a 'SAML_AUTH_SOURCE' változót kérdezi le.
- Default értékek kezelése (Ha a változó hiányzik és a default false, akkor exception: a változót kötelező megadni, ilyen lehet például egy jelszó)

Security Warning
================
It’s a lot easier to leak an environment variable than it is to leak a PHP variable.

- never leave phpinfo() call in production environment: it shows $_ENV and $_SERVER
- Don't leave debugging features in production environment: debugger may log the content of $_ENV and $_SERVER
- Don't expose in any other ways $_ENV and $_SERVER
- Don't let the webserver to serve .env file

Consider to disable phpinfo function in php.ini.

Virtualizáció, skálázhatóság
----------------------------
Ebben a példában a dockeres megfontolások nem kerültek kifejtésre, de megjegyezzük, hogy több példányban futó php
konténer esetén a runtime mappa közös köteten biztonságosan elhelyezhető.

Testreszabás, egyéni megoldások
-------------------------------

### Telepítésfüggő metaadatok elhelyezése

A `/config/simplesamlphp/metadata/private` directoryban a szabványos metaadatfájlok bármelyikét elhelyezhetjük (például `saml20-idp-remote.php` további választható manuális IdP bejegyzésekhez),
ez futásidőben belekerül a konfigurációba, de nem kerül bele a git repositoryba.


### Telepítésfüggő tesztfelhasználó adatok elhelyezése

A `/runtime/simplesaml/local-users.php` fájlban az ott található minafájl alapján elhelyezhetjük saját lokális tesztfelhasználóink adatait. Ez nem kerül bele a git repositoryba.  
