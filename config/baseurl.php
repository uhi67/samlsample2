<?php
/**
 * Returns the correct base URL for web and CLI environments.
 * Based on APP_BASEURL variable, but if not set, base URL is auto-detected.
 * CLI requires the correct protocol only if generates any output containing URLs.
 */
$https = $_SERVER['HTTPS'] ?? getenv('HTTPS');
if(php_sapi_name()=='cli' || getenv('APP_BASEURL')) {
    $baseurl = getenv('APP_BASEURL') ?: (
    ($https=='on' ? 'https' : 'http').'://' .
    getenv('APP_HOSTNAME') ?: 'gen.test'
    );
}
else {
    /** Reverse proxy protocol patch */
    $protocol = ($https == 'on' || ($_SERVER['SERVER_PORT'] ?? 80) == 443) ? "https" : "http";
    if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) || $https == 'on') {
        $protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $protocol;
        if($https == "on") $protocol = 'https';
        if($protocol == 'https') {
            $_SERVER['SERVER_PORT'] = 443;
            $_SERVER['HTTPS'] = 'on';
        }
    }
    $baseurl = $protocol . '://' . $_SERVER["HTTP_HOST"];
}
return $baseurl;
