<?php /** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnhandledExceptionInspection */

/**
 * Yii2 alkalmazás konfigurációs template
 * (csak a konfiguráció szempontjából releváns részek)
 */

use uhi67\envhelper\EnvHelper as EnvHelper;

// Check mandatory environment variables
$envVars = [
	'SIMPLESAMLPHP_CONFIG_DIR', // Ez nem írható felül a .env fájlban!
];
foreach($envVars as $envVar) {
	if(getenv($envVar)===false) {
		throw new Exception("Missing environment variable: $envVar");
	}
}

$config = [
    'id' => EnvHelper::getEnv('APP_NAME', 'samlsample2'),
	'name' => EnvHelper::getEnv('APP_TITLE', 'Sample Application'), # Azért van változóban, hogy a teszt verzió kiírhassa a címben, hgy ő a teszt
	'basePath' => dirname(__DIR__).'/',
	'language' => 'hu',
	'timeZone' => 'Europe/Budapest',
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm'   => '@vendor/npm-asset',
	],
	'bootstrap' => [
		'log',
	],
    'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => EnvHelper::getEnv('appCookieValidationKey'),
		],
		'urlManager' => [
			'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'enableStrictParsing' => false,
			'normalizer' => [
				'class' => 'yii\web\UrlNormalizer',
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
			'rules' => [
				'/' => 'site/index',
				'err' => 'site/err',
				'disabled' => 'site/disabled',
				'<action:(login|logout|log)>' => 'site/<action>',
				'doc/<id:[\w-]+>' => 'site/doc',
				'doc' => 'site/doc',
				'user/own' => 'site/own',
			],
		],
		'saml' => EnvHelper::getEnv('saml', [
			'class' => 'uhi67\eduidsp\Saml',
			'simpleSamlPath' => null,
			'authSource' => 'default-sp',
			'idAttribute' => 'eduPersonPrincipalName',
		]),
		'user' => [
			'class' => \app\components\AppWebUser::class,
			'identityClass' => \app\components\DummyUser::class,
			'enableSession' => true,  // default
			'enableAutoLogin' => true,
			'loginUrl' => ['site/mustlogin'],
		],
		'session' => [
			'class' => \yii\web\Session::class,
			'cookieParams' => [
				'httponly' => true,
				'secure' => YII_ENV_PROD,	// Ez http protokoll esetén gátolja a belépést.
				'lifetime' => 0,
				'sameSite' => PHP_VERSION_ID >= 70300 ? yii\web\Cookie::SAME_SITE_LAX : null,
			],
			'timeout' => 60 * 60 * 4,
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning', 'info'],
					'categories' => ['application'],
				],
			],
		],
		'assetManager' => [
			'linkAssets' => true,	// Uses symlinks, needs "Options FollowSymLinks" in vhost config
		],
    ],
];

return $config;
