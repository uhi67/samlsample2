<?php

/** @noinspection PhpUnhandledExceptionInspection */

use uhi67\envhelper\EnvHelper;

$idp = EnvHelper::getEnv('saml_idp', null);
/** @noinspection PhpUnhandledExceptionInspection */
$disco = EnvHelper::getEnv('saml_disco', null);
$authsource = EnvHelper::getEnv('saml_auth_source', 'default-sp');

$config = [

    // This is a authentication source which handles admin authentication.
    'admin' => [
        // The default is to use core:AdminPassword, but it can be replaced with
        // any authentication source.

        'core:AdminPassword',
    ],

    'localtest' => [
        'exampleauth:UserPass',
        'test0:test0' => [
            'uid' => ['test1'],
            'displayName' => 'test0',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test0@test.test',
            'mail' => 'test0@test.test',
            'niifPersonOrgID' => ['LOTE0AAA'],
        ],
        'test1:test1' => [
            'uid' => ['test1'],
            'displayName' => 'Teszt Elek',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test1@test.test',
            'mail' => 'test1@test.test',
            'niifPersonOrgID' => ['LOTE1AAA'],
        ],
        'test2:test2' => [
            'uid' => ['test2'],
            'displayName' => 'Teszt Kettes',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test2@test.test',
            'mail' => 'test2@test.test',
            'niifPersonOrgID' => ['LOTE2AAA'],
        ],
        'test3:test3' => [
            'uid' => ['test3'],
            'displayName' => 'Teszt Harmad',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test3@test.test',
            'mail' => 'test3@test.test',
            'niifPersonOrgID' => ['LOTE3AAA'],
        ],
        'test9:test9' => [
            'uid' => ['test9'],
            'displayName' => 'TesztEL9',
            'schacHomeOrganization' => 'pte.hu',
            'eduPersonAffiliation' => ['member', 'employee'],
            'eduPersonPrincipalName' => 'test9@test.test',
            'mail' => 'test9@test.test',
            'niifPersonOrgID' => ['LOTE9AAA'],
        ],
    ],

    $authsource => [
        'saml:SP',

        // The entity ID of this SP.
        // Can be NULL/unset, in which case an entity ID is generated based on the metadata URL.
        'entityID' => null,

        // The entity ID of the IdP this should SP should contact.
        // Can be NULL/unset, in which case the user will be shown a list of available IdPs.
        'idp' => $idp?:null,
        'disco' => $disco?:null,

        'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
        'authproc' => [
            91 => ['class' => 'core:AttributeMap', 'oid2name'],
        ],
    ],
];

$runtime = dirname(__DIR__, 3) .'/runtime/simplesaml';
if(file_exists($runtime.'/local-users.php')) {
    /** @noinspection PhpIncludeInspection
     * @noinspection RedundantSuppression -- a fájl nem mindig van
     */
    $users = require $runtime.'/local-users.php';
    $config['localtest'] = array_merge($config['localtest'], $users);
}
