<?php 

/**
*  Hungarian Research and Education Federation AttributeSchema representation
*  source: https://wiki.aai.niif.hu/images/3/35/99-niifschema.ldif
*  @author: Szabó Gyula, aai.sztaki.hu  <gyufi@sztaki.hu>
*  
*/

$attributemap = array(
	'urn:oid:1.3.6.1.4.1.11914.0.1.155' => 'niifPersonCityOfBirth',
	'urn:oid:1.3.6.1.4.1.11914.0.1.152' => 'niifPersonDateOfBirth',
	'urn:oid:1.3.6.1.4.1.11914.0.1.153' => 'niifPersonActivityStatus',
	'urn:oid:1.3.6.1.4.1.11914.0.1.169' => 'niifPersonJoinDate' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.154' => 'niifPersonOrgID',
	'urn:oid:1.3.6.1.4.1.11914.0.1.151' => 'niifCertificateSubjectDN',
	'urn:oid:1.3.6.1.4.1.11914.0.1.161' => 'niifEduPersonFacultyDN',
	'urn:oid:1.3.6.1.4.1.11914.0.1.167' => 'niifPersonPosition' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.1' => 'niifStatus',
	'urn:oid:1.3.6.1.4.1.11914.0.1.158' => 'niifPersonIdentityNumber',
	'urn:oid:1.3.6.1.4.1.11914.0.1.2' => 'niifTitle',
	'urn:oid:1.3.6.1.4.1.11914.0.1.173' => 'niifCertificateSHA1Fingerprint',
	'urn:oid:1.3.6.1.4.1.11914.0.1.164' => 'niifEduPersonAttendedCourse',
	'urn:oid:1.3.6.1.4.1.11914.0.1.171' => 'niifEduPersonArchiveCourse',
	'urn:oid:1.3.6.1.4.1.11914.0.1.172' => 'niifEduPersonHeldCourse',
	'urn:oid:1.3.6.1.4.1.11914.0.1.0' => 'niifPrefix',
	'urn:oid:1.3.6.1.4.1.11914.0.1.166' => 'niifPersonDegree' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.160' => 'niifEduPersonFaculty',
	'urn:oid:1.3.6.1.4.1.11914.0.1.162' => 'niifEduPersonMajor',
	'urn:oid:1.3.6.1.4.1.11914.0.1.170' => 'niifPersonQuitDate' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.157' => 'niifPersonMothersName',
	'urn:oid:1.3.6.1.4.1.11914.0.1.163' => 'niifEduPersonAcademicYear',
	'urn:oid:1.3.6.1.4.1.11914.0.1.156' => 'niifPersonCountyOfBirth',
	'urn:oid:1.3.6.1.4.1.11914.0.1.3' => 'niifUniqueId',
	'urn:oid:1.3.6.1.4.1.11914.0.1.165' => 'niifPersonPrefix' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.168' => 'niifActiveMemberOf' ,
	'urn:oid:1.3.6.1.4.1.11914.0.1.159' => 'niifPersonResidentalAddress',
	'urn:oid:1.3.6.1.4.1.11914.0.1.100' => 'niifIDPrefix',
);
