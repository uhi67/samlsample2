<?php 

/**
*  Hungarian Research and Education Federation AttributeSchema representation
*  source: https://wiki.aai.niif.hu/images/3/35/99-niifschema.ldif
*  @author: Szabó Gyula, aai.sztaki.hu  <gyufi@sztaki.hu>
*  
*/

$attributemap = array(
	'niifPersonCityOfBirth' => 'urn:oid:1.3.6.1.4.1.11914.0.1.155', 
 	'niifPersonDateOfBirth' => 'urn:oid:1.3.6.1.4.1.11914.0.1.152',
	'niifPersonActivityStatus' => 'urn:oid:1.3.6.1.4.1.11914.0.1.153',
	'niifPersonJoinDate'  => 'urn:oid:1.3.6.1.4.1.11914.0.1.169',
	'niifPersonOrgID' => 'urn:oid:1.3.6.1.4.1.11914.0.1.154',
	'niifCertificateSubjectDN' => 'urn:oid:1.3.6.1.4.1.11914.0.1.151',
	'niifEduPersonFacultyDN' => 'urn:oid:1.3.6.1.4.1.11914.0.1.161',
	'niifPersonPosition' => 'urn:oid:1.3.6.1.4.1.11914.0.1.167',
	'niifStatus' => 'urn:oid:1.3.6.1.4.1.11914.0.1.1',
	'niifPersonIdentityNumber' => 'urn:oid:1.3.6.1.4.1.11914.0.1.158',
	'niifTitle' => 'urn:oid:1.3.6.1.4.1.11914.0.1.2',
	'niifCertificateSHA1Fingerprint' => 'urn:oid:1.3.6.1.4.1.11914.0.1.173',
	'niifEduPersonAttendedCourse' => 'urn:oid:1.3.6.1.4.1.11914.0.1.164',
 	'niifEduPersonArchiveCourse' => 'urn:oid:1.3.6.1.4.1.11914.0.1.171',
	'niifEduPersonHeldCourse' => 'urn:oid:1.3.6.1.4.1.11914.0.1.172',
	'niifPrefix' => 'urn:oid:1.3.6.1.4.1.11914.0.1.0',
	'niifPersonDegree' => 'urn:oid:1.3.6.1.4.1.11914.0.1.166',
	'niifEduPersonFaculty' => 'urn:oid:1.3.6.1.4.1.11914.0.1.160',
	'niifEduPersonMajor' => 'urn:oid:1.3.6.1.4.1.11914.0.1.162',
	'niifPersonQuitDate' => 'urn:oid:1.3.6.1.4.1.11914.0.1.170',
	'niifPersonMothersName' => 'urn:oid:1.3.6.1.4.1.11914.0.1.157',
	'niifEduPersonAcademicYear' => 'urn:oid:1.3.6.1.4.1.11914.0.1.163',
	'niifPersonCountyOfBirth' => 'urn:oid:1.3.6.1.4.1.11914.0.1.156',
	'niifUniqueId' => 'urn:oid:1.3.6.1.4.1.11914.0.1.3',
	'niifPersonPrefix' => 'urn:oid:1.3.6.1.4.1.11914.0.1.165',
	'niifActiveMemberOf' => 'urn:oid:1.3.6.1.4.1.11914.0.1.168',
	'niifPersonResidentalAddress' => 'urn:oid:1.3.6.1.4.1.11914.0.1.159',
	'niifIDPrefix' => 'urn:oid:1.3.6.1.4.1.11914.0.1.100',
);
?>