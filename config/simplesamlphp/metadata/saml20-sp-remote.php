<?php /** @noinspection DuplicatedCode */
/**
 * SAML 2.0 remote SP metadata for simpleSAMLphp.
 * See: http://simplesamlphp.org/docs/trunk/simplesamlphp-reference-sp-remote
 */

/** @noinspection PhpUnhandledExceptionInspection */

use uhi67\envhelper\EnvHelper;

$baseurl = require dirname(__DIR__, 2).'/baseurl.php';
$authSource = EnvHelper::getEnv('saml_auth_source', 'default-sp');
$privacyStatementURL = getenv('SAML_PRIVACY') ?: $baseurl;

// itself as SP
$metadata[$baseurl.'/simplesaml/module.php/saml/sp/metadata.php/'.$authSource] = array (
    'AssertionConsumerService' => $baseurl.'/simplesaml/module.php/saml/sp/saml2-acs.php/'.$authSource,
    'SingleLogoutService' => $baseurl.'/simplesaml/module.php/saml/sp/saml2-logout.php/'.$authSource,
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    'PrivacyStatementURL' => ['hu'=>$privacyStatementURL],
    'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
);
