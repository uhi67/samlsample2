<?php
/** @noinspection DuplicatedCode */

/** @noinspection PhpUnhandledExceptionInspection */

use uhi67\envhelper\EnvHelper;

$baseurl = require dirname(__DIR__, 2).'/baseurl.php';
$appName = EnvHelper::getEnv('appName');

/*
 * PTE IdP
 */
$metadata['https://idp.pte.hu/saml2/idp/metadata.php'] = array(
    'name' => array(
        'hu' => 'Pécsi Tudományegyetem',
        'en' => 'University of Pécs, Hungary'
    ),
    'description'          => 'Here you can login with your account on PTE.',

    'SingleSignOnService'  => 'https://idp.pte.hu/saml/saml2/idp/SSOService.php',
    'SingleLogoutService'  => 'https://idp.pte.hu/saml/saml2/idp/SingleLogoutService.php',
    'certData'      => 'MIIEBjCCAu6gAwIBAgIJAO1KkTid1oWGMA0GCSqGSIb3DQEBBQUAMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odTAeFw0xMTEyMDkwODQwNTFaFw0yMTEyMDgwODQwNTFaMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANaBDaXoLcKixCE9ApBP/gYGJRTYlb4adtOSyXmNgz1Q3UnaEfvI/xsIg5bBJQpF/aRo4M95lETluYKUUko0mb1Gc5ENRomjY5EOtO0u231Et1HWfw0EUC8vMFIk1jW7A/RgBUJ5+iUuqfY/Y4d6wtle8YARSXAJUcDt2j4Ol1/TN70ybYSwtM12EeSwggH9zjnUP7C3euNqZKLKOY3rqH+f5zPVXnq4VOPBnxXVPFAvsyVFM3gKHXZkbfZKtD8YVUE0Mdxw8W1woYFGQ60GTe7wLA/gCPyyWD0TujJvSPb4DJdU46a9FlAkeAf7e5swskraEAnwhW9BQEQ1QFXIJ3cCAwEAAaOBxDCBwTAdBgNVHQ4EFgQUlMXISNOL71wzl9naHOlcluO+rQ8wgZEGA1UdIwSBiTCBhoAUlMXISNOL71wzl9naHOlcluO+rQ+hY6RhMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odYIJAO1KkTid1oWGMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBACND1KPdCd4VuwQWFfbquwvdaC8qD1KenQqpzB9KOR61nYUNQ/gMQDta8bybF/E+grjenx7Ttlny7IULCOtenZERbrI/li3RROz3PZDpHuZ+fZmJXqJVzo3lBUp1+lWqAnJGYJ6fBRKwaraeL26+gfX4huhxZbq5wrCjOaXglhUAZCgyBL7Zv0V9enrhgTz+3vGds69gKY1lyvY3a7PLB3OrygN293dxo/rAB9KaFLUDdK7K5JPWlie0a9Ck1bry3Pc2asdnf8QJh4PtcxQnK0wb7o23gJIIUs5/ckSYPXAUeFACvIteJVnpM0c0OQaplHPh37jHzJd0NgfnwoR4inU=',
);

/*
 * PTE teszt IdP
 */
$metadata['http://testidp.pte.hu/saml2/idp/metadata.php'] = array(
    'name' => array(
        'hu' => 'Pécsi Tudományegyetem Teszt',
        'en' => 'University of Pécs, Hungary, Test'
    ),
    'description'          => 'Here you can login with a test account on PTE.',

    'SingleSignOnService'  => 'http://testidp.pte.hu/saml/saml2/idp/SSOService.php',
    'SingleLogoutService'  => 'http://testidp.pte.hu/saml/saml2/idp/SingleLogoutService.php',
    'certData'      => 'MIIEBjCCAu6gAwIBAgIJAO1KkTid1oWGMA0GCSqGSIb3DQEBBQUAMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odTAeFw0xMTEyMDkwODQwNTFaFw0yMTEyMDgwODQwNTFaMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANaBDaXoLcKixCE9ApBP/gYGJRTYlb4adtOSyXmNgz1Q3UnaEfvI/xsIg5bBJQpF/aRo4M95lETluYKUUko0mb1Gc5ENRomjY5EOtO0u231Et1HWfw0EUC8vMFIk1jW7A/RgBUJ5+iUuqfY/Y4d6wtle8YARSXAJUcDt2j4Ol1/TN70ybYSwtM12EeSwggH9zjnUP7C3euNqZKLKOY3rqH+f5zPVXnq4VOPBnxXVPFAvsyVFM3gKHXZkbfZKtD8YVUE0Mdxw8W1woYFGQ60GTe7wLA/gCPyyWD0TujJvSPb4DJdU46a9FlAkeAf7e5swskraEAnwhW9BQEQ1QFXIJ3cCAwEAAaOBxDCBwTAdBgNVHQ4EFgQUlMXISNOL71wzl9naHOlcluO+rQ8wgZEGA1UdIwSBiTCBhoAUlMXISNOL71wzl9naHOlcluO+rQ+hY6RhMF8xCzAJBgNVBAYTAkhVMRAwDgYDVQQIEwdCYXJhbnlhMQ0wCwYDVQQHEwRQZWNzMQwwCgYDVQQKEwNQVEUxDDAKBgNVBAsTA0lJRzETMBEGA1UEAxMKaWRwLnB0ZS5odYIJAO1KkTid1oWGMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBACND1KPdCd4VuwQWFfbquwvdaC8qD1KenQqpzB9KOR61nYUNQ/gMQDta8bybF/E+grjenx7Ttlny7IULCOtenZERbrI/li3RROz3PZDpHuZ+fZmJXqJVzo3lBUp1+lWqAnJGYJ6fBRKwaraeL26+gfX4huhxZbq5wrCjOaXglhUAZCgyBL7Zv0V9enrhgTz+3vGds69gKY1lyvY3a7PLB3OrygN293dxo/rAB9KaFLUDdK7K5JPWlie0a9Ck1bry3Pc2asdnf8QJh4PtcxQnK0wb7o23gJIIUs5/ckSYPXAUeFACvIteJVnpM0c0OQaplHPh37jHzJd0NgfnwoR4inU=',
);

/*
 * Internal test auth. source
 */
$metadata[$baseurl.'/simplesaml/saml2/idp/metadata.php'] = array(
    'name' => array(
        'hu' => 'Helyi teszt bejelentkezés',
        'en' => 'Local test authentication source'
    ),
    'description'          => 'Here you can login with a local test account.',

    'SingleSignOnService'  => $baseurl.'/simplesaml/saml2/idp/SSOService.php',
    'SingleLogoutService'  => $baseurl.'/simplesaml/saml2/idp/SingleLogoutService.php',
    'certificate' => $appName.'.crt',
);
