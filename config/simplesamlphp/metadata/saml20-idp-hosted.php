<?php /** @noinspection DuplicatedCode */
/**
 * SAML 2.0 IdP configuration for simpleSAMLphp.
 *
 * See: https://rnd.feide.no/content/idp-hosted-metadata-reference
 */

/** @noinspection PhpUnhandledExceptionInspection */

use uhi67\envhelper\EnvHelper;

$baseurl = require dirname(__DIR__, 2).'/baseurl.php';
$appName = EnvHelper::getEnv('appName');
$orgScope = getenv('SAML_SCOPE') ?: 'sample.test';
$privacyStatementURL = getenv('SAML_PRIVACY') ?: $baseurl;

/*
    Test idp
*/
$metadata[$baseurl.'/simplesaml/saml2/idp/metadata.php'] = array(
    'host' => '__DEFAULT__',
    'auth' => 'localtest',
    'privatekey' => $appName.'.pem',
    'certificate' => $appName.'.crt',
    'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
    'AttributeNameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
    'userid.attribute' => 'uid', // (uid) Itt adjuk meg, hogy mely, az LDAPból származó attribútum alapján fogja az IdP kiszámítani az eduPersonTargetedID-t
    'OrganizationName' => array(
        'hu' => 'Helyi teszt felhasználók',
        'en' => 'Local Test Authentication Source'
    ),
    'OrganizationURL' => $orgScope,
    'authproc' => array(
        13 => array(
            'class' => 'core:AttributeAdd',
            'schacHomeOrganizationType' => array('urn:schac:homeOrganizationType:hu:university')
        ),
        // Example: eduPersonPrincipalName = test@pte.hu
        15 => array(
            'class' => 'core:ScopeAttribute',
            'scopeAttribute' => 'schacHomeOrganization',
            'sourceAttribute' => 'uid',
            'targetAttribute' => 'eduPersonPrincipalName',
            'onlyIfEmpty' => true,
        ),
        // example: eduPersonScopedAffiliation = [member@pte.hu, staff@pte.hu]
        30 => array(
            'class' => 'core:ScopeAttribute',
            'scopeAttribute' => 'schacHomeOrganization',
            'sourceAttribute' => 'eduPersonAffiliation',
            'targetAttribute' => 'eduPersonScopedAffiliation',
        ),
        31 => array(
            'class' => 'core:AttributeAdd',
            'o' => array('PTE')
        ),

        // eduPersonTargetedID előállítása az userid.attribute alapján
        40 => array(
            'class' => 'core:TargetedID',
            'nameId' => TRUE,	// SAML2 NameID formátum
        ),
    ),
    'attributeencodings' => array(
        'urn:oid:1.3.6.1.4.1.5923.1.1.1.10' => 'raw',
    ),
    'redirect.sign' => true,
    'PrivacyStatementURL' => $privacyStatementURL,
);
